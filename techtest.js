
//Creation du tableau, initialisation à vide ("")

row_number = 6;
column_number =7;

let tab = [];
let subtab = [];

function initialize(){
    for(var i=0; i < row_number; i++){
        for(var j=0; j < column_number; j++){
            subtab[j] = "";
        }
        tab[i] = subtab;
        subtab = [];
    }
}

initialize();

//pour vider le tableau html quand un jour a gagné
function initialize_html(){
    for(var i=0; i < row_number; i++){
        for(var j=0; j < column_number; j++){
            tableHTML.rows[i].cells[j].innerHTML = "";
        }
    } 
}

var tableHTML = document.getElementById("game"); //pour récuperer le tableau HTML et pouvoir le modifier en même temps que le tableau javascript

var tour_joueur = 0; //pour déterminer qui doit jouer (nombre pair pour le joueur 1 et impair pour le joueur 2)


var joueur1 = document.getElementById("p1");
var joueur2 = document.getElementById("p2");

//pour changer le contenu d'une case cliquer et mettre à jour pour savoir qui doit jouer 
function cellClick(row, column){
    var signe = "";
    if (tour_joueur%2 == 0){
        signe = "o";
    } else{
        signe = "x";
    }
    tableHTML.rows[row].cells[column].innerHTML = signe;
    tab[row][column] = signe;
    tour_joueur += 1;

    //pour afficher qui doit jouer
    if (tour_joueur%2 == 0){
        joueur1.innerHTML = "=> Player 1: o";
        joueur2.innerHTML = "Player 2: x";
    } else{
        joueur1.innerHTML = "Player 1: o";
        joueur2.innerHTML = "=> Player 2: x";
    }  
    winner_horizontal(signe);  
    winner_vertical(signe);
    winner_diagonal(signe, 0);
}


//on prend le signe en paramètre car on a besoin de vérifier que le jeton qui vient d'être joué
function winner_horizontal(signe){
    counter_horizontal = 0;
    for(var i=0; i< row_number; i++){
        for(var j=0; j<column_number; j++){
            //horizontal
            if(tab[i][j] == signe){
                counter_horizontal += 1;
                if (counter_horizontal == 4){
                    alert("Player \"" + signe+ "\" won");
                    initialize();
                    initialize_html();
                }
            }
            else{
                counter_horizontal = 0;
            }
        }
    }
}


function winner_vertical(signe){
    counter_vertical = 0;
    for(var i=0; i< column_number; i++){  //on inverse row et column par rapport à winner_horizontal
        for(var j=0; j<row_number; j++){

            //vertical
            if(tab[j][i] == signe){
                counter_vertical += 1;
                if (counter_vertical == 4){
                    alert("Player \"" + signe+ "\" won");
                    initialize();
                    initialize_html();
                }
            }
            else{
                counter_vertical = 0;
            }

        }
    }
}

function winner_diagonal(signe, counter){

    for(var i=0; i< row_number; i++){
        for(var j=0; j<column_number; j++){
            if(tab[i][j] == signe){
                checkDiagonal(signe, i, j, 1);
            }
        }
    }
}

function checkDiagonal(signe, i, j, counter){
    if (counter == 4){
        alert("Player \"" + signe+ "\" won");
        initialize();
        initialize_html();
    }

    //pour la diagonale "normale"
    else if (i+1 < row_number && j+1 < column_number && tab[i+1][j+1] == signe){
        console.log(counter);
        counter += 1;

        checkDiagonal(signe, i+1, j+1, counter);
    }

    //pour la diagonale "inversée"
    else if (i+1 < row_number && j-1 > 0 && tab[i+1][j-1] == signe){
        console.log(counter);
        counter += 1;
        checkDiagonal(signe, i+1, j-1, counter);
    }
}



//Bug et amélioration à programmer:
// Empecher de changer le contenu d'une cellule qui n'est pas vide
// Empecher de pouvoir remplir des cellules en haut du tableau si aucun jeton n'est dessous
// Le message du gagnant s'affiche avant que le pion gagnant soit afficher dans le tableau
//Le tableau se vide immédiatement après que l'alert est fermé

